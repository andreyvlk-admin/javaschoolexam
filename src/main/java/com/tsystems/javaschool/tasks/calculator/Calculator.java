package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.LinkedList;
import java.util.Locale;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        try {
            LinkedList<Double> storage = new LinkedList<>(); // number storage
            LinkedList<Character> operations = new LinkedList<>(); // We use it as stack for storing operations in FIFO order.
            // That's needed, because without it (with default locale) (in my case of course), there will be not a dot char.
            DecimalFormatSymbols symbols = new DecimalFormatSymbols(Locale.US);
            DecimalFormat formatter = new DecimalFormat("##.####", symbols); // create formatter, to return in needed style
            for (int i = 0; i < statement.length(); i++) {
                // We parse string with statement and calculate
                char c = statement.charAt(i);
                if (isDelimiter(c))
                    continue;
                if (c == '(')
                    operations.add('(');
                else if (c == ')') {
                    while (operations.getLast() != '(')
                        processOperator(storage, operations.removeLast());
                    operations.removeLast();
                } else if (isOperator(c)) {
                    while (!operations.isEmpty() && priority(operations.getLast()) >= priority(c))
                        processOperator(storage, operations.removeLast());
                    operations.add(c);
                } else {
                    // We use StringBuilder, to optimize time to complete. (because as I remember, when we concatenate String, we always cast original String to StringBuilder
                    // to reduce amount of casting in loop, we cast it before loop)
                    StringBuilder operand = new StringBuilder();
                    while (i < statement.length() && Character.isDigit(statement.charAt(i)) | statement.charAt(i) == '.') {
                        operand.append(statement.charAt(i++));
                    }
                    i--;
                    storage.add(Double.parseDouble(operand.toString()));
                }
            }
            while (!operations.isEmpty())
                processOperator(storage, operations.removeLast());
            // Because division by zero, returns infinite, we must check is it infinite (and in case if infinite, we must return null)
            return !storage.get(0).isInfinite() ? formatter.format(storage.get(0)) : null;
        } catch (Exception ex) {
            return null;
        }
    }

    private boolean isDelimiter(char c) {
        return c == ' ';
    }

    private boolean isOperator(char c) {
        // We return true if symbol equals to any stated math operation
        return c == '+' || c == '-' || c == '*' || c == '/' || c == '%';
    }

    private int priority(char operator) {
        // Here we set priority of operation.
        // When + or - we return 1, when * or / or % we return 2 in other cases we return -1
        switch (operator) {
            case '+':
            case '-':
                return 1;
            case '*':
            case '/':
            case '%':
                return 2;
            default:
                return -1;
        }
    }

    // We don't return anything, because we basically work with links to Objects.
    private void processOperator(LinkedList<Double> storage, char operation) {
        // We take last 2 elements from list and execute on them operation. (operation we chose by switch-case statement)
        double first = storage.removeLast();
        double second = storage.removeLast();
        switch (operation) {
            case '+':
                storage.add(second + first);
                break;
            case '-':
                storage.add(second - first);
                break;
            case '*':
                storage.add(second * first);
                break;
            case '/':
                storage.add(second / first);
                break;
            case '%':
                storage.add(second % first);
                break;
        }
    }

}
