package com.tsystems.javaschool.tasks.pyramid;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        try {
            if (inputNumbers.contains(null)) throw new CannotBuildPyramidException();
            Collections.sort(inputNumbers);
            int numberForPyramid = 0; // this number is created to check, do we have exactly needed amount of numbers to build a pyramid
            int numberHelper = 1; // this number help us to calculate numberForPyramid variable
            int rows = 0; // amount of rows in our pyramid
            // we find variables, and if numberForPyramid is bigger, than amount of elements, than that means that it's impossible to build pyramid, so we stop program, by throwing exception
            while (numberForPyramid != inputNumbers.size()) {
                if (numberForPyramid > inputNumbers.size()) throw new CannotBuildPyramidException();
                numberForPyramid += numberHelper;
                numberHelper += 1;
                rows++;
            }
            ArrayList<ArrayList<Integer>> pyramid = provideListWithGivenNumbers(inputNumbers, rows);
            zeroFiller(pyramid, rows);
            return castListToArray(pyramid);
        } catch (OutOfMemoryError ex) {
            throw new CannotBuildPyramidException();
        }

    }

    private int[][] castListToArray(ArrayList<ArrayList<Integer>> pyramid) {
        int[][] arr = new int[pyramid.size()][pyramid.get(0).size()];
        for (int i = 0; i < pyramid.size(); i++) {
            arr[i] = pyramid.get(i).stream().mapToInt(x -> x).toArray();
        }
        return arr;
    }

    // We don't return anything, because we basically work with links to Objects.
    private ArrayList<ArrayList<Integer>> provideListWithGivenNumbers(List<Integer> inputNumbers, int rows) {
        ArrayList<ArrayList<Integer>> pyramid = new ArrayList<>();
        ArrayList<Integer> rowNumbers = new ArrayList<>();
        int numberPosition = inputNumbers.size() - 1; // we use it as counter-indexer.
        for (int i = rows; i > 0; i--) {
            for (int j = i; j > 0; j--) {
                rowNumbers.add(inputNumbers.get(numberPosition--));
            }
            Collections.reverse(rowNumbers);
            pyramid.add(rowNumbers);
            rowNumbers = new ArrayList<>();
        }
        Collections.reverse(pyramid);
        return pyramid;
    }

    // We don't return anything, because we basically work with links to Objects.
    private void zeroFiller(ArrayList<ArrayList<Integer>> pyramid, int rows) {
        // We fill right and left side with zero, with usage of so-called by me "pyramid pattern" that I found out while thinking how to solve this task.
        for (ArrayList<Integer> row : pyramid) {
            for (int j = 1; j < rows; j++) {
                row.add(0, 0);
                row.add(0);
            }
            rows--;
        }
        // We fill with zero places, where non-zero numbers go one by another. (again "pyramid pattern")
        for (ArrayList<Integer> integers : pyramid) {
            for (int j = 0; j < integers.size() - 1; j++) {
                if (integers.get(j) != 0 && integers.get(j + 1) != 0) {
                    integers.add(j + 1, 0);
                }
            }
        }
    }

}
